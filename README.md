## How to run it

### STEP 1: Configure environment and install dependencies

For simplicity all you need has been saved in `.env.example`, just rename it to `.env`

```
$ composer install
```

Publish the assets

```
$ php artisan vendor:publish --tag=ckeditor
```

Link storage to public directory

```
$ php artisan storage:link
```

### STEP 2: Run MySQL docker container

```
$ docker run --name mycmsdb -e MYSQL_ROOT_PASSWORD=secret -p 33066:3306 -d mariadb:10
```

Install also PHPMyAdmin like so

```
$ docker run --name myadmin -d --link mycmsdb:db -p 8484:80 phpmyadmin
```

Navigate to URL: http://localhost:8484

Enter user "root" and password "secret"

If you rather want to access the db from CLI use the following command

```
$ docker exec -it mycmsdb mysql -hlocalhost -uroot -psecret
```

### STEP 3: Generate the database schema and seed

Create a database called `mycms` via PHPMyAdmin.

Run migrations:

```
$ php artisan migrate
```

To seed the database use the "Import" feature of PHPMyAdmin and load the sql file `example_data.sql`

### STEP 4: Run the PHP server

```
$ cd public
$ php -S localhost:8080
```

### STEP 5: try out the application

Navigate to: http://localhost:8080

Try login as user: `test@test.com` with password: `secret`


